import { TimePickerWithDropdown } from "./components/timePickerWithDropdown";

export default function App() {
  const handleTimeChange = (selectedTime) => {
    console.log("Selected time:", selectedTime.format("h:mm A"));
  };

  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "100vh",
      }}
    >
      <TimePickerWithDropdown
        timeSlot="15"
        noOfTimeSlots="32"
        onTimeChange={handleTimeChange}
      />
    </div>
  );
}
