import { useState } from "react";
import { LocalizationProvider, TimePicker } from "@mui/x-date-pickers";
import dayjs from "dayjs";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { MenuItem, Select, Radio } from "@mui/material";

import AccessTimeIcon from "@mui/icons-material/AccessTime";

export const TimePickerWithDropdown = ({
  timeSlot,
  noOfTimeSlots,
  ...props
}) => {
  const now = dayjs();
  const defaultDate =
    now.hour() < 6 || (now.hour() === 6 && now.minute() < 45)
      ? now.startOf("day").add(6, "hour")
      : now.startOf("day").add(1, "day").add(6, "hour");

  const timeOptions = [
    { value: "05:45", label: "5:45 AM" },
    ...Array.from({ length: ((noOfTimeSlots - 1) * 60) / 30 }, (_, i) => {
      const time = defaultDate.add(i * timeSlot, "minute");
      return { value: time.format("HH:mm"), label: time.format("h:mm A") };
    }),
  ];

  const [selectedTime, setSelectedTime] = useState(defaultDate);

  const handleTimeChange = (event) => {
    const time = dayjs(event.target.value, "HH:mm");
    setSelectedTime(time);
    if (typeof props.onTimeChange === "function") {
      props.onTimeChange(time);
    }
  };

  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <div
        style={{
          border: "0.5px solid black",
        }}
      >
        <TimePicker
          value={selectedTime}
          onChange={(newTime) => {
            setSelectedTime(newTime);
            if (typeof props.onTimeChange === "function") {
              props.onTimeChange(newTime);
            }
          }}
          disablePast
          sx={{
            width: "110px",
            position: "absolute",
            background: "white",
            zIndex: 1,
            boxShadow: "none",
            borderRadius: "6px",
            ".MuiOutlinedInput-notchedOutline": { border: 0 },
            "& .Mui-focused": {
              backgroundColor: "transparent",
              boxShadow: "none",
            },
            "& .MuiInputBase-input": {
              width: "100px",
              textAlign: "center",
            },
            "& .MuiInputBase-root.Mui-focused .MuiOutlinedInput-notchedOutline":
              {
                borderColor: "transparent",
              },
          }}
        />
        <Select
          value=""
          onChange={handleTimeChange}
          IconComponent={() => <AccessTimeIcon color="black" />}
          MenuProps={{
            anchorOrigin: {
              vertical: "bottom",
              horizontal: "center",
            },
            getContentAnchorEl: null,
            PaperProps: {
              style: {
                maxHeight: 250,
                width: 200,
              },
            },
          }}
          sx={{
            border: "0px",
            width: "200px",
            boxShadow: "none",
            background: "white",
            padding: "0 14px 0 0",
            ".MuiOutlinedInput-notchedOutline": { border: 0 },
            outline: "none",
            "& fieldset": {
              border: "none",
            },
            "&:focus:not(.Mui-disabled) .MuiOutlinedInput-notchedOutline": {
              border: "none",
            },
          }}
        >
          {timeOptions.map((option) => (
            <MenuItem key={option.value} value={option.value}>
              <Radio
                checked={selectedTime.format("HH:mm") === option.value}
                onChange={handleTimeChange}
                color="primary"
                name="radio-button-time"
                inputProps={{ "aria-label": option.label }}
                sx={{
                  height: "10px",
                  "&.Mui-checked": {
                    color: "black",
                  },
                }}
              />

              {option.label}
            </MenuItem>
          ))}
        </Select>
      </div>
    </LocalizationProvider>
  );
};
